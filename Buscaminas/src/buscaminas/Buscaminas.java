package buscaminas;

import java.util.*;

/**
 * <h2>classe Buscaminas, es un joc en el que tens que descobrir totes les
 * caselles sense descobrir una mina, l'unica manera de saber on estan les mines
 * es prestant atenci� als numeros de les caselles, els numeros ens indiquen
 * quantes mines estan tocant aquesta casella, ja sigui en horitzontal, en
 * vertical o en diagonal.</h2>
 * 
 * Busca informaci�n de Javadoc en <a href="http://www.google.com">GOOGLE</a>
 * 
 * @see <a href="http://www.google.com"> Google</a>
 * @version 1.0
 * @author Oriol Porta Sobrino
 * @since 1.0
 */

public class Buscaminas {

	static Scanner sc = new Scanner(System.in);
	static Random rnd = new Random();

	public static void main(String[] args) {

		int[] tamano;
		int menu, minas, difi;
		boolean redi;
		tamano = new int[2];
		redi = false;
		difi = 0;
		minas = 0;
		menu = 1;
		while (menu != 0) {
			verMenu();
			menu = introducirDatos(Integer.MIN_VALUE + 1, Integer.MAX_VALUE, "Selecciona una opci�");
			switch (menu) {
			case 0:
				System.out.println("Fi del programa");
				break;
			case 1:
				mostrarAyuda();
				break;
			case 2:
				difi = dificultad();
				minas = opciones(tamano, difi);
				redi = true;
				break;
			case 3:
				if (!redi)
					System.out.println("Primer has de escollir les opcions");
				else
					juego(tamano, minas, difi);
				break;
			case 4:
				rankings(0, -1);
				break;
			default:
				System.out.println("El numero ha de estar entre 0 i 4");
			}
		}
	}

	/**
	 * Aquest metode serveix per escollir la dificultat
	 * 
	 * @return La dificultat seleccionada
	 */

	public static int dificultad() {
		int dif;
		dif = 0;
		while (dif < 1 || dif > 4) {
			verMenuOpciones();
			dif = introducirDatos(Integer.MIN_VALUE + 1, Integer.MAX_VALUE, "Selecciona una opci�");
		}
		return dif;
	}

	/**
	 * Aquest metode es el que porta la partida sencera
	 * 
	 * @param tamano Tamany del mapa tant en files([0]) com en columnes ([1])
	 * @param minas  Quantitat de mines
	 * @param difi   Dificultat seleccionada
	 */

	public static void juego(int[] tamano, int minas, int difi) {
		long inicio, fin;
		int cont, aux;
		int[][] visible;
		char[][] invisible;
		boolean cen;
		cen = true;
		visible = new int[tamano[0]][tamano[1]];
		invisible = new char[tamano[0]][tamano[1]];
		inicializarTablero(visible, tamano[0], tamano[1]);
		visualizarTablero(visible, tamano[0], tamano[1]);
		inicio = System.currentTimeMillis();
		cont = primerClic(visible, invisible, tamano[0], tamano[1], minas);
		while (tamano[0] * tamano[1] - minas > cont && cen) {
			visualizarTablero(visible, tamano[0], tamano[1]);
			aux = turno(visible, invisible, tamano[0], tamano[1]);
			if (aux == -1)
				cen = false;
			if (cen)
				cont += aux;
		}
		if (cen) {
			fin = System.currentTimeMillis();
			fin = (fin - inicio) / 1000;
			System.out.println("Has ganado!");
			if (difi != 4)
				rankings(difi, fin);
		}
	}

	/**
	 * Aques metode estableix les mides que has seleccionat segons la dificultat
	 * 
	 * @param tamano Tamany del mapa
	 * @param difi   Dificultat escollida
	 * @return Retorna la quantitat de mines que tindra al mapa
	 */

	public static int opciones(int[] tamano, int difi) {
		int minas;
		minas = 0;
		switch (difi) {
		case 1:
			tamano[0] = 8;
			tamano[1] = 8;
			minas = 10;
			break;
		case 2:
			tamano[0] = 16;
			tamano[1] = 16;
			minas = 40;
			break;
		case 3:
			tamano[0] = 16;
			tamano[1] = 30;
			minas = 99;
			break;
		case 4:
			tamano[0] = introducirDatos(8, 24, "Introdueix el numero de files ha de estar entre 8 i 24");
			tamano[1] = introducirDatos(8, 32, "Introdueix el numero de columnes ha de estar entre 8 i 32");
			minas = numeroBombas(tamano[0], tamano[1]);
			break;
		default:
			System.out.println("El numero ha de estar entre 1 i 4");
		}
		return minas;
	}

	/**
	 * Aquest metode s'encarrega de fer el primer clic el qual creara les bombes
	 * 
	 * @param visible   Mapa visible per el jugador
	 * @param invisible Mapa invisible on s'amaguen les bombes
	 * @param filas     Numero de files del mapa
	 * @param columnas  Numero de columnes del mapa
	 * @param bombas    Numero de bombes del mapa
	 * @return Retorna la cuantitat de caselles que son visibles (serveix per a
	 *         comprobar si hass guanyat o no)
	 */

	public static int primerClic(int[][] visible, char[][] invisible, int filas, int columnas, int bombas) {
		int fila, columna, cont;
		fila = introducirDatos(0, filas - 1, "Introdueix fila (ha de estar entre 0 i " + (filas - 1) + ")");
		columna = introducirDatos(0, columnas - 1, "Introdueix columna (ha de estar entre 0 i " + (columnas - 1) + ")");
		ponerBombas(invisible, filas, columnas, fila, columna, bombas);
		ponerNumeros(invisible, filas, columnas);
		for (int i = 0; i < filas; i++) {
			System.out.printf("%4d", i);
			for (int j = 0; j < columnas; j++)
				System.out.printf("%2c ", invisible[i][j]);
			System.out.println();
		}
		cont = limpiar(visible, invisible, fila, columna);
		return cont;
	}

	/**
	 * Metode que serveix per a introduir dades
	 * 
	 * @param min   El numero minim de la dada a introduir
	 * @param max   El numero maxim de la dada a introduir
	 * @param texto El text que sortira per a saber que has d'introduir
	 * @return Retornara la dada quan aquesta sigui correcta
	 */

	public static int introducirDatos(int min, int max, String texto) {
		int dato;
		dato = Integer.MIN_VALUE;
		while (dato < min || dato > max) {
			System.out.println(texto);
			try {
				dato = sc.nextInt();
			} catch (Exception e) {
				System.out.println("Numero invalid");
				sc.nextLine();
			}
		}
		return dato;
	}

	/**
	 * Aquest metode serveix per a veure el menu principal del joc
	 */

	public static void verMenu() {
		System.out.println("   0: Sortir");
		System.out.println("   1: Mostrar ajuda");
		System.out.println("   2: Opcions");
		System.out.println("   3: Jugar");
		System.out.println("   4: Veure Ranking");
	}

	/**
	 * Aquest metode serveix per a veure les diferents dificultats del joc
	 */

	public static void verMenuOpciones() {
		System.out.println("   1: Principiant (8*8 caselles i 10 mines)");
		System.out.println("   2: Intermitg (16*16 caselles i 40 mines)");
		System.out.println("   3: Expert (16*30 caselles i 99 mines)");
		System.out.println("   4: Personalitzat");
	}

	/**
	 * Serveix per omplir el mapa visible
	 * 
	 * @param visible  Mapa que veu el jugador
	 * @param filas    Numero de files del mapa
	 * @param columnas Numero de columnes del mapa
	 */

	public static void inicializarTablero(int[][] visible, int filas, int columnas) {
		int i, j;
		for (i = 0; i < filas; i++)
			for (j = 0; j < columnas; j++)
				visible[i][j] = 9;
	}

	/**
	 * Per a visualitzar el tauler i veure com avan�a
	 * 
	 * @param tablero  Tauler visible del joc
	 * @param filas    Numero de files del mapa
	 * @param columnas Numero de columnes del mapa
	 */

	public static void visualizarTablero(int[][] tablero, int filas, int columnas) {
		int i, j;
		System.out.print("\n\n    ");
		for (j = 0; j < columnas; j++)
			System.out.printf("%2d ", j);
		System.out.println();
		for (i = 0; i < filas; i++) {
			System.out.printf("%4d", i);
			for (j = 0; j < columnas; j++)
				System.out.printf("%2d ", tablero[i][j]);
			System.out.println();
		}
	}

	/**
	 * Per a introduir el numero de bombes quan has seleccionat la dificultat
	 * personalitzada
	 * 
	 * @param filas    Numero de files del mapa
	 * @param columnas Numero de columnes del mapa
	 * @return Retorna la quantitat de bombes introduides
	 */

	public static int numeroBombas(int filas, int columnas) {
		int max, bombas;
		max = (filas * columnas) / 3;
		bombas = introducirDatos(1, max,
				"Introdueix les bombes que vols a la teva partida (han de anar de 1 a " + max + ")");
		return bombas;
	}

	/**
	 * Posa les bombes als llocs adequats del mapa
	 * 
	 * @param invisible Mapa invisible on es possen les mines
	 * @param filas     Numero de files del mapa
	 * @param columnas  Numero de columnes del mapa
	 * @param fila      Fila escollida per el primer clic
	 * @param columna   Columna escollida per el primer clic
	 * @param bombas    Mines totals a possar
	 */

	public static void ponerBombas(char[][] invisible, int filas, int columnas, int fila, int columna, int bombas) {
		int i, j, k, fil, col;
		boolean cen;
		i = 0;
		while (i < bombas) {
			cen = false;
			fil = rnd.nextInt(filas);
			col = rnd.nextInt(columnas);
			if (invisible[fil][col] != '*') {
				cen = true;
				j = fila - 1;
				while (j <= fila + 1 && cen) {
					k = columna - 1;
					while (k <= columna + 1 && cen) {
						if (fil == j && col == k)
							cen = false;
						k++;
					}
					j++;
				}
			}
			if (cen) {
				invisible[fil][col] = '*';
				i++;
			}
		}

	}

	/**
	 * Mostra la casella on has clicat i descobreix els espais buits del voltant si
	 * n'hi ha
	 * 
	 * @param visible   Mapa que veu el jugador
	 * @param invisible Mapa que oculta les mines
	 * @param fila      Fila a analitzar si s'ha de descobrir
	 * @param columna   Columna a analitzar si s'ha de descobrir
	 * @return Retorna la quantitat de caselles desobertes
	 */

	public static int limpiar(int[][] visible, char[][] invisible, int fila, int columna) {
		int i, j, cont;
		cont = 0;
		for (i = fila - 1; i <= fila + 1; i++)
			for (j = columna - 1; j <= columna + 1; j++) {
				if (i >= 0 && i < visible.length && j >= 0 && j < visible[0].length)
					if (invisible[i][j] == '0' && (i != fila || j != columna) && visible[i][j] == 9)
						cont += limpiar(visible, invisible, i, j);
					else if (visible[i][j] == 9) {
						if (invisible[i][j] != '*') {
							visible[i][j] = invisible[i][j] - '0';
							cont++;
						}
					}
			}
		return cont;
	}

	/**
	 * Posa els numeros corresponents al mapa invisible
	 * 
	 * @param invisible Mapa que conte les mines i numeros i que el jugador no pot
	 *                  veure
	 * @param filas     Numero de files del mapa
	 * @param columnas  Numero de columnes del mapa
	 */

	public static void ponerNumeros(char[][] invisible, int filas, int columnas) {
		int i, j, k, l;
		char num;
		for (i = 0; i < filas; i++) {
			for (j = 0; j < columnas; j++) {
				num = '0';
				if (invisible[i][j] != '*') {
					for (k = i - 1; k <= i + 1; k++)
						for (l = j - 1; l <= j + 1; l++)
							if (k >= 0 && l >= 0 && k < filas && l < columnas && invisible[k][l] == '*')
								num++;
					invisible[i][j] = num;
				}
			}
		}
	}

	/**
	 * Aquest metode gestiona cada torn del joc
	 * 
	 * @param visible   Mapa amb tota la infromacio secreta que el jugador no veu
	 * @param invisible Mapa que veu el jugador
	 * @param filas     Numero de files del mapa
	 * @param columnas  Numero de columnes del mapa
	 * @return Retorna el numero de caselles descobertes per a saber si has guanyat
	 *         o no
	 */

	public static int turno(int[][] visible, char[][] invisible, int filas, int columnas) {
		int fila, columna, cont;
		fila = introducirDatos(0, filas - 1, "Introdueix una fila (entre 0 i " + (filas - 1) + ")");
		columna = introducirDatos(0, columnas - 1, "Introdueix una columna (entre 0 i " + (columnas - 1) + ")");
		cont = 0;
		if (invisible[fila][columna] == '*') {
			System.out.println("Has perdut");
			cont = -1;
		} else {
			if (visible[fila][columna] == 9)
				cont = limpiar(visible, invisible, fila, columna);
		}
		return cont;
	}

	/**
	 * Aquest metode mostra una breu explicacio del joc del Buscamines
	 */

	public static void mostrarAyuda() {
		System.out.println("El buscamines es un joc de 1989 amb una mecanica bastant sencilla \n"
				+ "hi ha un numero determinat de mines escampades per el mapa, \n"
				+ "si fas clic a alguna d'aquestes has perdut, l'objectiu es \n"
				+ "descobrir totes les caselles que no tenen mines, per fer aixo \n"
				+ "t'has de fixar en els numeros de les caselles, cada numero \n"
				+ "significa el numero de mines que estan tocant aquesta casella \n"
				+ "ja sigui horitzontal, vertical o diagonal");
	}

	/**
	 * Mostra el ranking de la dificultat "Principiant"
	 * 
	 * @param fin Puntuacio en temps de la partida
	 */

	public static void principiante(long fin) {
		long[] top = new long[5];
		String[] topnombre = new String[5];
		boolean cen = true;
		String nombre;
		int i;
		i = 0;
		System.out.print("Principiant\n\n");
		while (i < 5 && cen) {
			if ((fin < top[i] || top[i] == 0) && fin >= 0) {
				cen = false;
				top[i] = fin;
				System.out.println("Has quedado en la posicion " + (i + 1) + " del ranking!");
				System.out.println("Introduce tus siglas: ");
				sc.nextLine();
				nombre = introducirNombre();
				topnombre[i] = nombre;
			}
			if (topnombre[i] != null) {
				System.out.print((i + 1) + "   ");
				System.out.print(topnombre[i] + "...........");
				System.out.println(top[i]);
			}
			i++;
		}
	}

	/**
	 * Metode per a introduir les teves sigles al ranking
	 * 
	 * @return Retorna les sigles introduides
	 */

	public static String introducirNombre() {
		String nom;
		int i;
		boolean cen;
		nom = "";
		cen = false;
		while (!cen) {
			nom = sc.nextLine();
			if (nom.length() != 3)
				System.out.println("Las siglas tienen que ser 3 caracteres");
			else {
				cen = true;
				i = 0;
				while (cen && i < 3) {
					if (nom.charAt(i) > 'A' && nom.charAt(i) < 'Z') {
					}

					else
						cen = false;
					i++;
				}
				System.out.println("Les sigles han de ser majuscules i nom�s lletres");
			}
		}
		return nom;
	}

	/**
	 * Mostra el menu del ranking
	 */

	public static void menuRanking() {
		System.out.println("1:     Principiant");
		System.out.println("2:     Intermitg");
		System.out.println("3:     Expert");
	}

	/**
	 * Et deixa escollir quin el ranking de quina dificultat vols veure, en cas que
	 * hagis jugat et ficara directament a la teva dificultat per a comprovar si
	 * entres al ranking
	 * 
	 * @param menu Entres al ranking de la dificultat selleccionada, si vens del
	 *             menu principal aquest valor sera sempre 0 i et deixara
	 *             seleccionar un ranking
	 * @param fin  Puntuacio feta per a comprovar amb la resta de jugadors del
	 *             ranking, si vens desde el menu principal aquest valor sera sempre
	 *             -1
	 */

	public static void rankings(int menu, long fin) {
		menuRanking();
		if (menu == 0)
			menu = introducirDatos(Integer.MIN_VALUE + 1, Integer.MAX_VALUE, "Selecciona una opci�");
		switch (menu) {
		case 1:
			principiante(fin);
			break;
		case 2:
			intermedio(fin);
			break;
		case 3:
			experto(fin);
			break;
		default:
			System.out.println("El numero ha de estar entre 1 i 3");
		}
	}

	/**
	 * Mostra el ranking de la dificultat "Intermig"
	 * 
	 * @param fin Puntuacio en temps de la partida
	 */

	public static void intermedio(long fin) {
		long[] top = new long[5];
		String[] topnombre = new String[5];
		boolean cen = true;
		String nombre;
		int i;
		i = 0;
		System.out.print("Intermitg\n\n");
		while (i < 5 && cen) {
			if ((fin < top[i] || top[i] == 0) && fin >= 0) {
				cen = false;
				top[i] = fin;
				System.out.println("Has quedado en la posicion " + (i + 1) + " del ranking!");
				System.out.println("Introduce tus siglas: ");
				sc.nextLine();
				nombre = introducirNombre();
				topnombre[i] = nombre;
			}
			if (topnombre[i] != null) {
				System.out.print((i + 1) + "   ");
				System.out.print(topnombre[i] + "...........");
				System.out.println(top[i]);
			}
			i++;
		}
	}

	/**
	 * Mostra el ranking de la dificultat "Expert"
	 * 
	 * @param fin Puntuacio en temps de la partida
	 */

	public static void experto(long fin) {
		long[] top = new long[5];
		String[] topnombre = new String[5];
		boolean cen = true;
		String nombre;
		int i;
		i = 0;
		System.out.print("Expert\n\n");
		while (i < 5 && cen) {
			if ((fin < top[i] || top[i] == 0) && fin >= 0) {
				cen = false;
				top[i] = fin;
				System.out.println("Has quedado en la posicion " + (i + 1) + " del ranking!");
				System.out.println("Introduce tus siglas: ");
				sc.nextLine();
				nombre = introducirNombre();
				topnombre[i] = nombre;
			}
			if (topnombre[i] != null) {
				System.out.print((i + 1) + "   ");
				System.out.print(topnombre[i] + "...........");
				System.out.println(top[i]);
			}
			i++;
		}
	}

}