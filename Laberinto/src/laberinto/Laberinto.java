package laberinto;

import java.util.*;

/**
 * <h2>clase Laberinto, es un joc amb un mapa ple de mines i parets en les que introdueixes 
 * les instruccions que ha de seguir el jugador a dins, un cop acabades les instruccions et diu la resolucio 
 * del programa que es si ha sortit, si ha morta i per on ha sortit (en cas que hagi sortit) </h2>
 * 
 * Busca informacion de Javadoc en <a href="http://www.google.com">GOOGLE</a>
 * @see <a href="http://www.google.com"> Google</a>
 * @version 1.0
 * @author Oriol Porta Sobrino
 * @since 1.0
 */

public class Laberinto {

	static Scanner sc = new Scanner(System.in);
	static final int FILAS = 9;
	static final int COLUMNAS = 9;
	static final int[][] MAPAS = { 
			{ 2, 2, 1, 1, 3, 1, 3, 1, 2 }, 
			{ 1, 1, 1, 1, 2, 1, 1, 1, 1 }, 
			{ 2, 1, 3, 1, 1, 1, 1, 1, 2 },
			{ 1, 1, 2, 1, 1, 1, 2, 1, 1 }, 
			{ 2, 3, 1, 1, 0, 1, 1, 2, 1 }, 
			{ 1, 1, 1, 1, 1, 1, 2, 1, 2 },
			{ 2, 2, 1, 3, 2, 1, 1, 1, 3 }, 
			{ 1, 1, 1, 2, 1, 1, 1, 3, 1 }, 
			{ 3, 2, 1, 1, 1, 2, 1, 1, 3 } };

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int casos, i;
		casos = introducirDatos();
		sc.nextLine();
		for (i = 0; i < casos; i++) {
			personaje();
		}

	}
	
	/**
	 * Aqui es on tracta cada cas
	 */

	public static void personaje() {
		String instruccion;
		char dir;
		int dis, largo, i;
		boolean vivo;
		int[] pos;
		int [][] mapa;
		pos = new int[2];
		pos[0] = 4;
		pos[1] = 4;
		mapa=iniciarTablero(MAPAS);
		vivo=true;
		dis = 0;
		dir ='0';
		instruccion = sc.nextLine();
		instruccion=instruccion.toUpperCase();
		largo = instruccion.length();
		i=0;
		while (i < largo && vivo) {
			if (instruccion.charAt(i) >= '0' && instruccion.charAt(i) <= '9')
				dis = instruccion.charAt(i)-'0';
			else
				dir = instruccion.charAt(i);
			if (i % 2 != 0)
				vivo=comprobar(mapa, dir, dis, pos);
			i++;
		}
		if (vivo)
			System.out.println("T'has perdut");
	}
	
	/**
	 * Metode per introduir dades
	 * @return retorna el numero entrat per consola
	 */

	public static int introducirDatos() {
		int num;
		num = sc.nextInt();
		return num;
	}
	
	/**
	 * Comproba la direccio i el numero de pasos
	 * @param mapa Mapa del joc
	 * @param dir Direccio a caminar
	 * @param dis Caselles a camniar
	 * @param pos Posicio actual
	 * @return  Estat del jugador (viu/mort)
	 */

	public static boolean comprobar(int[][] mapa, char dir, int dis, int []pos) {
		int i;
		int[] paso = new int[2];
		boolean vivo, ganar;
		ganar=false;
		vivo = true;
		switch (dir) {
		case 'N':
			if (vivo)
				paso[0] = -1;
			break;
		case 'S':
			if (vivo)
				paso[0] = 1;

			break;
		case 'E':
			if (vivo)
				paso[1] = 1;
			break;
		case 'O':
			if (vivo)
				paso[1] = -1;
			break;
		}
		i=0;
		while (i < dis && vivo && !ganar) {
			try {								
				if (mapa[pos[0] + paso[0]][pos[1] + paso[1]]>0) {}					
				} catch (ArrayIndexOutOfBoundsException e) {
					ganar=true;
				}
			if (vivo && !ganar) 
				vivo = mover(mapa, pos, paso);			
			i++;
		}
		if (!vivo || ganar)
			vivo=resultado(vivo, dir);
		return vivo;
	}
	
	/**
	 * Mou una casella cap a una direccio
	 * @param mapa El mapa del llaberint
	 * @param pos Posicio a la que moure's
	 * @param paso El pas que dona aquest moviment
	 * @return Retorna si esta viu o mort
	 */

	public static boolean mover(int[][] mapa, int[] pos, int[] paso) {
		boolean vivo;
		vivo = true;
		switch (mapa[pos[0] + paso[0]][pos[1] + paso[1]]) {
		case 1:
			mapa[pos[0] + paso[0]][pos[1] + paso[1]] = 0;
			mapa[pos[0]][pos[1]] = 1;
			pos[0]+=paso[0];
			pos[1]+=paso[1];
			break;
		case 2:
			break;
		case 3:
			vivo = false;
			break;
		}
		return vivo;
	}
	
	/**
	 * Mostra el resultat per pantalla
	 * @param vivo Si esta viu o mort
	 * @param dir La direccio per on ha escapat
	 * @return retonra sempre false perque ja ha acabat el recorregut
	 */
	
	public static boolean resultado(boolean vivo, char dir) {
		if (vivo)
			switch (dir) {
			case 'N': 
				System.out.println("Has escapat por el Nord");
				break;
			case 'S':
				System.out.println("Has escapat por el Sud");
				break;
			case 'E':
				System.out.println("Has escapat por l'Est");
				break;
			case 'O':
				System.out.println("Has escapat por l'Oest");
				break;
			}
		else 
			System.out.println("BOOM");
		vivo=false;
		return vivo;
	}
	
	/**
	 * Inicialitza el tauler
	 * @param mapas El mapa del llaberint
	 * @return retorna el mapa restaurat
	 */
	
	public static int[][] iniciarTablero(int [][]mapas) {
		int i, j;
		int [][] local = new int [FILAS][COLUMNAS];
		for (i=0;i<FILAS;i++)
			for (j=0;j<COLUMNAS;j++) {
				local[i][j]=mapas[i][j];
			}
		return local;
	}

}
