package laberintoTest;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import laberinto.Laberinto;

@RunWith(Parameterized.class)

public class LaberintoTestComprobar {
	
	private int [][] mapa;
	private char dir;
	private int dis;
	private int [] pos;
	private boolean resul;
	
	public LaberintoTestComprobar (int [][] mapa, char dir, int dis, int [] pos, boolean resul) {
		this.mapa = mapa;
		this.dir = dir;
		this.dis = dis;
		this.pos = pos;
		this.resul = resul;
	}

	@Parameters
	public static Collection <Object[]> numeros() {
		return Arrays.asList (new Object[][] {
			{new int[][] {{1,1,3},{1,0,1},{2,1,1}}, 'N', 1, new int[] {1,1}, true},	
			{new int[][] {{1,1,1,1,1,1},{1,1,1,1,1,1},{2,1,1,1,1,1}}, 'E', 4, new int[] {1,1}, true},
			{new int[][] {{1,3,1,1,2,1},{3,1,1,2,1,1},{3,1,1,1,1,1}}, 'O', 5, new int[] {2,5}, false},	
			{new int[][] {{1},{1},{2},{1},{1},{1},{1},{1}}, 'S', 7, new int[] {0,0}, true},	
			{new int[][] {{1},{1},{3},{1},{1},{1},{1},{1}}, 'S', 11, new int[] {0,0}, false},	
		});
		
	}

	@Test
	public void testComprobar() {
		boolean res = Laberinto.comprobar(mapa, dir, dis, pos);
		assertEquals(res, resul);
		//fail("Not yet implemented");
	}

}
